import React from 'react';
import ButtonsBottom from '../components/ButtonsBottom';
import ContactForm from '../components/ContactForm';
import Logo from '../components/Logo';
import Mouse from '../components/Mouse';
import Navigation from '../components/Navigation';
import SocialNetwork from '../components/SocialNetwork';
import { motion} from 'framer-motion';

const Contact = () => {
    const variants = {
        in: {
            opacity: 1,
            x:0
        },
        out: {
            opacity: 0,
            x: 300
        }
    }
    const transition = {
        ease: [0.03, 0.87, 0.73, 0.9],
        duration: 0.6
    }
     return(
         <main>
             <Mouse/>
            <motion.div
             className="contact"
             exit="out"
             animate="in"
             initial="out"
             variants={variants}
             transition={transition}
             >
                <Navigation/>
                <Logo/>
                 <ContactForm/>
                <div className="contact-infos">
                    <div className="address">
                        <div className="content">
                        <h4>adresse</h4>
                        <p>155 rue des bateliers </p>
                        <p>26730 Eymeux</p>
                        </div>  
                    </div>
                
           
            <div className="phone"> 
               <div className="content">
                   <h4>Téléphone</h4>
                   <p>0649380762</p>
               </div>
            </div>
            <div className="email">
                <div className="content">
                    <h4>email</h4>
                    <p>antonylemaire26730@gmail.com</p>
                </div>

            </div>
            <SocialNetwork/>
            <div className="credits">
                <p>from antony lemaire 2020</p>
            </div>
            </div>
            <ButtonsBottom left={'/project-4'}/>
        </motion.div>
            </main>
     );
};

export default Contact;