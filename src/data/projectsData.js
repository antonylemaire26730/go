export const projectsData = [
  {
    id: 1,
    title: "L'art des choix",
    date: "juillet 2020",
    languages: ["React", "Php", "Sass"],
    infos:
      "Site pour un fastfood basé au cheylard.",
    img: "./assets/img/projet.png",
    link: "http://www.google.com",
  },
  {
    id: 2,
    title: "Site perso",
    date: "Mars 2020",
    languages: ["Symfony", "Vue"],
    infos:
      "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quas cumque labore suscipit, pariatur laboriosam autem omnis saepe quisquam enim iste.",
    img: "./assets/img/projet-1.png",
    link: "http://www.google.com",
  },
  {
    id: 3,
    title: "Spidzza Grand mére",
    date: "Avril 2020",
    languages: ["Wordpress", "Php", "woocommerce"],
    infos:
      "Site ecommerce de vente de d'habit pour animaux",
    img: "./assets/img/projet-3.png",
    link: "https://www.spidzza.com/",
  },
  {
    id: 4,
    title: "Biblio",
    date: "Juillet 2020",
    languages: ["Android studio", "Kotlin"],
    infos:
      "Application pour classer mes livres ",
    link: "https://gitlab.com/antonylemaire26730/biblio",
  },
];
